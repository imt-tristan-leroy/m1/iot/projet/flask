from apscheduler.schedulers.background import BackgroundScheduler
import database
import captor
from app import app

def get_captor_data():
    with app.app_context():
        db = database.get()
        c = captor.get()

        captor_time = c.get_time()
        captor_state = c.get_state()

        db.input_register.create(captor_time, captor_state)

        password = c.get_password()
        night_password = c.get_night_password()

        db.holding_register.create(captor_time, password, night_password)


def start():
    sched = BackgroundScheduler(daemon=True)
    sched.add_job(get_captor_data, 'interval', minutes=1)
    sched.start()
