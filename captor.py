import input_register
import holding_register
import random
import datetime
from flask import g


def get():
	if 'captor' not in g:
		# g.captor = Captor(input_register.get(), holding_register.get())
		g.captor = Captor(None, None)

	return g.captor


def close(e=None):
	captor = g.pop('captor', None)


class Captor:
	def __init__(self, input_registry, holding_registry):
		self.input_register = input_registry
		self.holding_register = holding_registry
		
		# TODO: Remove var when connected
		self.password = "9510"
		self.night_password = "0159"

	# TODO: Connect to the captor
	def get_password(self):
		return self.password

	# TODO: Connect to the captor
	def get_night_password(self):
		return self.night_password

	# TODO: Connect to the captor
	def get_time(self):
		date_time = datetime.datetime.now()
		return date_time

	# TODO: Connect to the captor
	def get_state(self):
		return random.choice(["IDLE", "WAITING PASSWORD", "VALIDATING PASSWORD", "ERROR PASSWORD", "WAITING NIGHT PASSWORD", "VALIDATING NIGHT PASSWORD", "ERROR NIGHT PASSWORD", "BLOCKED"])

	# TODO: Connect to the captor
	def update_password(self, password):
		self.password = password

	# TODO: Connect to the captor
	def update_night_password(self, password):
		self.night_password = password