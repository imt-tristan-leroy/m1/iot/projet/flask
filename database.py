import datetime
import sqlite3
from flask import current_app, g


def get():
	if 'db' not in g:
		g.db = DB()
		g.db.init_schema()
	return g.db


def close(e=None):
	db = g.pop('db', None)

	if db is not None:
		db.close()


class DB:
	def __init__(self):
		self.connection = sqlite3.connect(current_app.config["DATABASE"])
		self.input_register = InputRegister(self.connection)
		self.holding_register = HoldingRegister(self.connection)

	def init_schema(self):
		self.input_register.init_schema()
		self.holding_register.init_schema()

	def close(self):
		self.connection.close()


class InputRegister:
	def __init__(self, connection):
		self.connection = connection

	def init_schema(self):
		cursor = self.connection.cursor()
		cursor.execute('''
			CREATE TABLE IF NOT EXISTS input_register(
				id			 INTEGER    PRIMARY KEY    AUTOINCREMENT,
				captor_date    INTEGER    NOT NULL,
				captor_state   TEXT       NOT NULL,
				computer_date  INTEGER    NOT NULL       DEFAULT CURRENT_TIMESTAMP
			);
			''')
		self.connection.commit()
		cursor.close()

	def create(self, captor_date, state):
		cursor = self.connection.cursor()
		cursor.execute('''
			INSERT INTO input_register(captor_date, captor_state)
			VALUES (?, ?);
		''', (captor_date.timestamp(), state))
		self.connection.commit()
		cursor.close()

	def get_all(self, limit=0, offset=0):
		cursor = self.connection.cursor()
		if limit > 0:
			res = cursor.execute('''
				SELECT *
				FROM input_register
				LIMIT ?
				OFFSET ?;
			''', (limit, offset))
		else:
			res = cursor.execute('''
				SELECT *
				FROM input_register;
			''')
		registers = []
		for register in res.fetchall():
			register = list(register)
			register[1] = datetime.date.fromtimestamp(register[1])
			registers.append(register)
		cursor.close()
		return registers

	def get_one(self, identifiant):
		cursor = self.connection.cursor()
		res = cursor.execute('''
			SELECT *
			FROM input_register
			WHERE id == ?;
		''', identifiant)
		cursor.close()
		data = res.fetchone()
		data = list(data)
		data[1] = datetime.date.fromtimestamp(data[1])
		return


class HoldingRegister:
	def __init__(self, connection):
		self.connection = connection

	def init_schema(self):
		cursor = self.connection.cursor()
		cursor.execute('''
			CREATE TABLE IF NOT EXISTS holding_register(
				id			 	INTEGER    PRIMARY KEY    AUTOINCREMENT,
				captor_date     INTEGER    NOT NULL,
				password   		TEXT       NOT NULL,
				night_password  TEXT       NOT NULL,
				computer_date  	INTEGER    NOT NULL       DEFAULT CURRENT_TIMESTAMP
			);
			''')
		self.connection.commit()
		cursor.close()

	def create(self, captor_date, password, night_password):
		cursor = self.connection.cursor()
		cursor.execute('''
			INSERT INTO holding_register(captor_date, password, night_password)
			VALUES (?, ?, ?);
		''', (captor_date, password, night_password))
		self.connection.commit()
		cursor.close()

	def get_all(self, limit=0, offset=0):
		cursor = self.connection.cursor()
		if limit > 0:
			res = cursor.execute('''
				SELECT *
				FROM holding_register
				LIMIT ?
				OFFSET ?;
			''', (limit, offset))
		else:
			res = cursor.execute('''
				SELECT *
				FROM holding_register;
			''')
		registers = []
		for register in res.fetchall():
			register = list(register)
			#register[1] = datetime.date.fromtimestamp(register[1])
			registers.append(register)
		cursor.close()
		return registers

	def get_one(self, identifiant):
		cursor = self.connection.cursor()
		res = cursor.execute('''
			SELECT *
			FROM holding_register
			WHERE id == ?;
		''', identifiant)
		cursor.close()
		data = res.fetchone()
		data = list(data)
		data[1] = datetime.date.fromtimestamp(data[1])
		return data