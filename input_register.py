import serial_port
from enum import Enum
from flask import current_app, g


def get():
	if 'input_register' not in g:
		serial = serial_port.get()
		g.input_register = InputRegister(serial)

	return g.input_register


def close(e=None):
	input_register = g.pop('input_register', None)


class Address(Enum):
	YEAR = 1

	@staticmethod
	def valid(address):
		if address.value in Address.__members__:
			current_app.logger.log("An error occurred. Expected command to be one of %, Got: %", list(Address), address)
			return False
		return True


class Function(Enum):
	READ = 6


class InputRegister:
	def __init__(self, serial):
		self.serial = serial

	def read(self, address, number, slave=1):
		if address.value in Address.__members__:
			current_app.logger.log("An error occurred. Expected command to be one of %, Got: %", list(Address), address)
			return None

		slave = f"{slave}".rjust(2, "0")
		function = f"{Function.READ.value}".rjust(2, "0")
		address = f"{address.value}".rjust(4, "0")
		number = f"{number}".rjust(4, "0")

		content = f"{slave}{function}{address}{number}"
		self.serial.send(content)
		return self.serial.receive()
