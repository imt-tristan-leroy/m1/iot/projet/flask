# Flask IHM Projet IOT

## Description

### Flask 

A basic front end that display data from the captor and help update the passwords in the captor

### Scheduler

A scheduler get the captor data every minutes

## Requirements

- python 3.9 

## Installation

```sh
$ pip install -r ./requirements.txt
```
## Run

```sh
$ python3 ./app.py
```