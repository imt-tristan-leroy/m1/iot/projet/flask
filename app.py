from flask import Flask, render_template, redirect, flash, request
import database
import captor
import holding_register
import input_register
import serial_port
import scheduler

app = Flask(__name__)
app.config.from_object('config')
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'


@app.teardown_appcontext
def shutdown(exception):
	database.close()
	captor.close()
	input_register.close()
	holding_register.close()
	serial_port.close()


@app.route('/')
def home():
	db = database.get()
	c = captor.get()

	input_registers = db.input_register.get_all()
	holding_registers = db.holding_register.get_all()

	password = c.get_password()
	night_password = c.get_night_password()

	return render_template('pages/home.html', input_registers=input_registers, holding_registers=holding_registers, password=password, night_password=night_password)


@app.route('/read-captor', methods=['POST'])
def read_captor():
	db = database.get()
	c = captor.get()

	captor_time = c.get_time()
	captor_state = c.get_state()

	db.input_register.create(captor_time, captor_state)

	password = c.get_password()
	night_password = c.get_night_password()

	db.holding_register.create(captor_time, password, night_password)

	return redirect('/')

@app.route('/update-password', methods=['POST'])
def update_password():
	password = request.form.get("password", False)
	nightPassword = request.form.get("night_password", False)

	if not password:
		flash('password is required!', "danger")
	if not nightPassword:
		flash('night password is required!', "danger")
	
	c = captor.get()

	c.update_password(password)
	c.update_night_password(nightPassword)

	return redirect('/')


# ----------------------------------------------------------------------------#
# Launch.
# ----------------------------------------------------------------------------#

# Default port:
if __name__ == '__main__':
	# scheduler.start()
	app.run()
